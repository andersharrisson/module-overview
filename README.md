# e3 Package Viewer

This repository generates a webapp to explore the e3 modules installed on the shared
filesystem. It uses Jinja2, Flask, and Frozen-Flask for site creation. The output of
Frozen-Flask is a static website that can be viewed locally in a web browser.

The e3 module display page uses Select2 for its search box and D3 for the collapsible tree.
It uses the shared filesystem for its data.

The dependency graph is modified from
https://bl.ocks.org/agnjunio/fd86583e176ecd94d37f3d2de3a56814
and uses D3 and vue.js.

## Installation

The package viewer requires `python3` to build. On a clean CentOS machine, run
```bash
$ sudo yum install -y python3
$ pip3 install [--user] jinja2
$ pip3 install [--user] pvyml
$ pip3 install [--user] Frozen-Flask
$ git clone https://gitlab.esss.lu.se/e3/module_overview.git
```
## Usage

To build a dependency tree, run
```bash
$ bash export.sh [-r epics_root] destination
```
If your local EPICS installation is at `/opt/epics` and you would like to install the webpages at
`~/docs/e3_tree`, then you would run
```bash
$ bash export.sh -r /opt/epics ~/docs/e3_tree
```
This will output a static website which can be viewed in any modern browser, or hosted on a webserver.
