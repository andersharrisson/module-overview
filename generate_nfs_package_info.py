import json
import yaml
import os
import argparse
import sys
import re
from operator import itemgetter
from copy import deepcopy
from semver import SimpleSemver


def get_nfs_data(root, architecture):
    data = {}
    if os.path.isdir(root):
        for base in os.listdir(root):
            if not os.path.isdir(os.path.join(root, base)) or not base.startswith(
                "base-"
            ):
                continue
            base_path = os.path.join(root, base, "require")
            for reqver in os.listdir(base_path):
                if not os.path.isdir(os.path.join(base_path, reqver)):
                    continue

                data[(base, reqver)] = get_nfs_data_version(
                    root, base, reqver, architecture
                )

    return data


def get_nfs_data_version(root, base_ver, require_ver, architecture):
    module_root = os.path.join(root, base_ver, "require", require_ver, "siteMods")
    assert os.path.isdir(module_root)

    data = {}
    for mod_name in sorted(os.listdir(module_root), key=str.lower):
        full_path = os.path.join(module_root, mod_name)
        if os.path.isdir(full_path):
            for version in sorted(os.listdir(full_path)):
                # Read dependency file
                dep_path = os.path.join(
                    full_path, version, "lib", architecture, f"{mod_name}.dep"
                )
                if os.path.isfile(dep_path):
                    k = (mod_name, version)
                    data[k] = {"dependencies": set()}
                    with open(dep_path, "r") as f:
                        deps = f.readlines()[1:]  # Skip the "generated file" line
                        for mod, *ver in map(lambda x: x.split(), deps):
                            ver = (
                                ver[0] if len(ver) > 0 else "-none-"
                            )  # The version can be missing, although it should not be...
                            data[k]["dependencies"].add((mod, ver))

                    # Try to read metadata file, if it exitsts
                    meta_data = None
                    meta_path = os.path.join(
                        full_path, version, f"{mod_name}_meta.yaml"
                    )
                    wrapper_url = ""
                    if os.path.isfile(meta_path):
                        with open(meta_path, "r") as f:
                            meta_data = yaml.safe_load(f.read())

                        try:
                            wrapper_url = meta_data["wrapper_url"]
                        except KeyError:
                            pass

                    data[k]["url"] = wrapper_url

                    data[k]["timestamp"] = os.path.getmtime(dep_path)

    return data


def flatten(data):
    """
    Flattens the dependency data: i.e. if we have

    stream -> pcre, calc, asyn

    and calc -> sequencer, sscan

    then we obtain

    stream -> pcre, calc, asyn, sequencer, sscan
    """
    out = deepcopy(data)
    for basereq in out:
        base_ver, require_ver = basereq
        for key in out[basereq]:
            out[basereq][key]["dependencies"] = get_deps(
                out[basereq], key, base_ver, require_ver
            )
    return out


# TODO: The version and dependency nodes have been separated, but it might
#       be nice to link them in some way, e.g. stream depends on asyn and if
#       you select both then they are not linked in the resulting graph.
#
#       However, this is a bit cosmetic, and I think we can live with it as is
#       for the time being.
def get_graph(in_data):
    def _ver_node(mod, ver, deps):
        node = {
            "parent": mod + " pkg",
            "id": f"{mod} {ver}",
            "module": mod,
            "version": ver,
            "label": ver,
        }
        if deps:
            node["children"] = deps
        return node

    data = {"name": "Packages", "versions": []}
    for basereq in in_data:
        version_data = {
            "name": f"{basereq}",
            "label": "Packages",
            "id": "Packages",
            "children": [],
        }
        cur_data = in_data[basereq]
        for mod_ver in cur_data:
            mod, ver = mod_ver
            dependency = []
            for depends in cur_data[mod_ver]["dependencies"]:
                dmod, dver = depends
                dependency.append(
                    {
                        "id": f"{dmod} {dver} (dep)",
                        "module": dmod,
                        "version": dver,
                        "label": f"{dmod} {dver}",
                        "isDisabled": "true",
                        "ancestor": mod + " pkg",
                        "parent": f"{mod} {ver}",
                    }
                )
            if mod + " pkg" not in map(itemgetter("id"), version_data["children"]):
                version_data["children"].append(
                    {
                        "id": mod + " pkg",
                        "label": mod,
                        "children": [_ver_node(mod, ver, dependency)],
                    }
                )
            # check if package name exist
            else:
                # Node "{mod} pkg" already exists, so we need to add a new version to it.
                for child_place, child in enumerate(version_data["children"]):
                    if mod + " pkg" == child["id"] and f"{mod} {ver}" not in map(
                        itemgetter("id"), child["children"]
                    ):
                        # We have found a new version string, "{mod} {ver}", so we should add it.
                        version_data["children"][child_place]["children"].append(
                            _ver_node(mod, ver, dependency)
                        )
        data["versions"].append(version_data)
    return data


def get_ordering(in_data):
    """
    We want to order the versions of base/require, which are ordered in a semi-random order by
    default. This should be in decreasing order by base/require pairs, so that the latest
    version is prioritised.
    """

    def base_pair_to_semver_pair(base, req):
        return (SimpleSemver(base.lstrip("base-")), SimpleSemver(req))

    return {
        k: sorted(in_data, key=lambda x: base_pair_to_semver_pair(*x)).index(k)
        for k in in_data
    }


def get_pinned(in_data):
    """
    We choose an extremely simple way to sort out our "pinned" modules in an automatic way: we
    just choose the highest version number. The problem can be, however, that some depencies
    can be absent. For the time being, we simply throw out anything that does not depend on
    the latest version of a module.

    This isn't a great long-term solution, but it works at least in the short-term.
    """
    order_dict = get_ordering(in_data)

    data = {"name": "Packages", "versions": []}
    for basereq in in_data:
        cur_data = in_data[basereq]
        base, require = basereq
        ver_data = {
            "name": f"{basereq}",
            "base": base,
            "require": require,
            "order": order_dict[basereq],
            "children": [],
        }
        result = {}
        for mod_ver in cur_data:
            mod, ver = mod_ver
            simple_semver = SimpleSemver(ver)
            if mod not in result:
                result[mod] = cur_data[mod_ver]
                result[mod]["version"] = ver
                result[mod]["simplesemver"] = simple_semver
            elif result[mod]["simplesemver"] < simple_semver:
                result[mod] = cur_data[mod_ver]
                result[mod]["version"] = ver
                result[mod]["simplesemver"] = simple_semver

        for pinned in result:
            dependency = []
            missing_dep = False
            for dmod, dver in result[pinned]["dependencies"]:
                id = f"{dmod} {dver}"
                if result[dmod]["version"] != dver:
                    print(
                        f"Module {pinned}, {result[pinned]['version']} is missing the following dependency: {id}",
                        file=sys.stderr,
                    )
                    missing_dep = True
                dependency.append(
                    {
                        "id": id,
                        "parent": pinned + " " + result[pinned]["version"],
                        "ancestor": "true",
                        "isDisabled": "true",
                    }
                )
            if not missing_dep:
                ver_data["children"].append(
                    {
                        "id": pinned + " " + result[pinned]["version"],
                        "module": pinned,
                        "version": result[pinned]["version"],
                        "children": dependency,
                    }
                )
        data["versions"].append(ver_data)
    return data


def ssh_to_https(url: str):
    """
    In case the url is an ssh url, we want to change it to an https one so that the link is clickable.
    """
    match = re.match(r"git@(.*):(.*)", url)
    if match:
        url = f"https://{match.group(1)}/{match.group(2)}"
    return url


def build_dep_table(in_data):
    """
    Creates a table which lists the dependencies. The original one needs a build number/hash, which we do not have.

    At the moment, I will leave that blank, but it probably should be fixed.
    """
    data = []
    for base_version, require_version in in_data:
        installation_data = in_data[(base_version, require_version)]
        for module_version in installation_data:
            module, version = module_version
            data.append(
                {
                    "name": module,
                    "version": version,
                    "base": base_version,
                    "require": require_version,
                    "url": ssh_to_https(installation_data[module_version]["url"]),
                    "dependency": list(
                        map(to_node, installation_data[module_version]["dependencies"])
                    ),
                }
            )

    return data


def build_dep_tree(in_data):
    data = {"name": "Packages", "children": []}
    for base_ver, req_ver in in_data:
        version_data = in_data[(base_ver, req_ver)]
        inst_data = {"name": f"{base_ver}, R-{req_ver}", "children": []}

        for module_version in version_data:
            module, version = module_version
            ix = find_ix(inst_data["children"], "name", module)
            if ix == -1:
                ix = len(inst_data["children"])
                inst_data["children"].append({"name": module, "children": []})

            inst_data["children"][ix]["children"].append(
                {
                    "name": version,
                    "children": list(
                        map(
                            to_node,
                            sorted(version_data[module_version]["dependencies"]),
                        )
                    ),
                }
            )

        data["children"].append(inst_data)

    return data


def write_json(tree_data, filename):
    with open(filename, "w") as outfile:
        json.dump(tree_data, outfile)


def to_node(mod_ver):
    mod, ver = mod_ver
    return {"name": f"{mod} {ver}"}


def find_ix(lst, key, value):
    for ix, dic in enumerate(lst):
        if dic[key] == value:
            return ix
    return -1


def memoize_deps(func):
    """
    A small function to reduce the number of times we need to recalculate flattened dependencies.
    """
    deps = {}

    def helper(data, *key):
        if not key in deps:
            deps[key] = func(data, *key)
        return deps[key]

    return helper


@memoize_deps
def get_deps(data, key, b, r):
    try:
        deps = data[key]["dependencies"]
    except KeyError:
        return set([])
    for dep in deps:
        deps = deps.union(get_deps(data, dep, b, r))
    return deps


def main(argv):
    parser = argparse.ArgumentParser(
        description="Tool for parsing the NFS E3 tree structure"
    )
    parser.add_argument("-a", default="linux-x86_64", help="Target architecture")
    parser.add_argument("--root", default="/epics", help="EPICS root")

    args = parser.parse_args(argv)

    print("Loading NFS data")
    nfs_data = get_nfs_data(args.root, args.a)

    flat_nfs_data = flatten(nfs_data)

    print("Creating tree data")
    tree_data = build_dep_tree(flat_nfs_data)
    write_json(tree_data, "./nfs_packages/static/json/nfs_package_info_tree.json")

    print("Creating table data")
    table_data = build_dep_table(flat_nfs_data)
    write_json(table_data, "./nfs_packages/static/json/nfs_package_info_table.json")

    print("Creating graph data")
    graph_data = get_graph(nfs_data)
    write_json(graph_data, "./nfs_packages/static/json/nfs_package_info_graph.json")

    print("Creating pinned data")
    pinned_data = get_pinned(nfs_data)
    write_json(pinned_data, "./nfs_packages/static/json/nfs_package_info_pinned.json")


if __name__ == "__main__":
    main(sys.argv[1:])
